# language: no

Egenskap: Brukerhistorie 1
  Scenario: 1.1 Legge til varer i handlekurven
  Gitt at jeg har åpnet nettkiosken
  Når jeg legger inn varer og kvanta
  Så skal handlekurven inneholde det jeg har lagt inn
  Og den skal ha riktig totalpris

  Scenario: 1.2 Slett varer
  Gitt at jeg har åpnet nettkiosken
  Og lagt inn varer og kvanta
  Når jeg sletter varer
  Så skal ikke handlekurven inneholde det jeg har slettet
  
  Scenario: 1.3 Oppdater varer
  Gitt at jeg har åpnet nettkiosken
  Og lagt inn varer og kvanta
  Når jeg oppdaterer kvanta for en vare
  Så skal handlekurven inneholde riktig kvanta for varen