import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// Scenario 2.1
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').clear().type('Jovana Petkovic');
    cy.get('#address').clear().type('Min adresse her');
    cy.get('#postCode').clear().type('4008');
    cy.get('#city').clear().type('Stavanger');
    cy.get('#creditCardNo').clear().type('1111222233334444');
});

And(/^trykker på Fullfør kjøp$/, function () {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').contains('Din ordre er registrert.');
});

// Scenario 2.2
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
      // implementerer kun navn håndering, resten er kopiering og liming..
      // Sjekk navn
      cy.get('#fullName').clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('form').submit();
    cy.get('#fullNameError').should('have.text', 'Feltet må ha en verdi');
});